# Flatpak for Citra Emulator
## Installation
Note: You need the [Flathub repo](https://flathub.org/setup) first and `org.flatpak.Builder` installed, as well as the sdk and extensions used by the manifest.
```bash
flatpak run org.flatpak.Builder --user --install build-dir io.github.PabloMK7.Citra.yml
```
## Updating
Since the upstream project is shotdown, i will try to update the manifest when necessary and/or when a new project comes off to maintain the source.
Contributions are welcome
